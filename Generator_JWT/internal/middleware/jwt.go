package middleware

import (
	"crypto/hmac"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var SECRET = []byte("dimas3207211611960001")

func CreateJWT() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(time.Hour * 2).Unix()

	tokenStr, err := token.SignedString(SECRET)

	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return tokenStr, nil
}

func ValidateHmac(signatureClient string, serverSignature string) bool {
	cekHmac := hmac.Equal([]byte(signatureClient), []byte(serverSignature))

	return cekHmac
}
