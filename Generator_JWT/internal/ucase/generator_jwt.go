package ucase

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
)

type getToken struct{}

func GenerateToken() *getToken {
	return &getToken{}
}

func (gt getToken) Serve(data *appctx.Data) appctx.Response {
	if data.Request.Header["Id"] == nil {
		return *appctx.NewResponse().
			WithStatus("Failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusBadRequest).
			WithMessage("x_api_key_id not found")
	}
	Timestamp := data.Request.Header["Timestamp"][0]
	x_api_key_id := data.Request.Header["Id"][0]
	body, err := ioutil.ReadAll(data.Request.Body)
	if err != nil {
		panic(err)
	}

	bodyRaw := md5.Sum([]byte(body))
	finalBody := string(bodyRaw[:])
	body_md5 := base64.StdEncoding.EncodeToString([]byte(finalBody))

	hmac_signature := Timestamp + ":" + x_api_key_id + ":" + data.Request.Method + ":" + body_md5

	secret := "dimas3207211611960001"

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(hmac_signature))
	hmac_base64 := base64.StdEncoding.EncodeToString(h.Sum(nil))

	signature := "#" + x_api_key_id + ":#" + hmac_base64
	serverSignature := base64.StdEncoding.EncodeToString([]byte(signature))
	clientSignature := data.Request.Header["Signature"][0]

	Hmac := middleware.ValidateHmac(serverSignature, clientSignature)

	if !Hmac {
		return *appctx.NewResponse().
			WithStatus("Failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusBadRequest).
			WithMessage("Signature Unauthorized")
	}

	token, err := middleware.CreateJWT()

	if err != nil {
		return *appctx.NewResponse().
			WithStatus("error generating token").
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	dataResponse := map[string]interface{}{
		"type":       "bearer",
		"token":      token,
		"expired_at": time.Now().Add(time.Hour * 2),
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithStatus("Success").
		WithEntity("generateToken").
		WithState("generateTokenSuccess").
		WithMeta(meta).
		WithData(dataResponse)

}
